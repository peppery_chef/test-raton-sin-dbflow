package cl.palacios.www.ratontest2;

public class Usuario {

    private int id;
    private String nombre;
    private String apellido;
    private String correo;
    private String user;
    private String pass;
    private boolean usuarioactivo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public boolean isUsuarioactivo() {
        return usuarioactivo;
    }

    public void setUsuarioactivo(boolean usuarioactivo) {
        this.usuarioactivo = usuarioactivo;
    }
}
