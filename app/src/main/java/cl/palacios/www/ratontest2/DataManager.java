package cl.palacios.www.ratontest2;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static cl.palacios.www.ratontest2.DataManager.DB_NAME;
import static cl.palacios.www.ratontest2.DataManager.DB_VERSION;
import static cl.palacios.www.ratontest2.DataManager.TABLA_USUARIO;
import static cl.palacios.www.ratontest2.DataManager.USUARIO_NOMBRE;
import static cl.palacios.www.ratontest2.DataManager.USUARIO_APELLIDO;
import static cl.palacios.www.ratontest2.DataManager.USUARIO_CORREO;
import static cl.palacios.www.ratontest2.DataManager.USUARIO_ID;
import static cl.palacios.www.ratontest2.DataManager.USUARIO_PASS;
import static cl.palacios.www.ratontest2.DataManager.USUARIO_USER;
import static cl.palacios.www.ratontest2.DataManager.USUARIO_USUARIOACTIVO;

public class DataManager {

    private static SQLiteDatabase db;
    private mySQLiteOpenHelper helper;

    public static final String TABLA_USUARIO="Usuario";

    public static final String USUARIO_ID="_id";
    public static final String USUARIO_NOMBRE ="nombre";
    public static final String USUARIO_APELLIDO="apellido";
    public static final String USUARIO_CORREO="correo";
    public static final String USUARIO_USER="user";
    public static final String USUARIO_PASS="pass";
    public static final String USUARIO_USUARIOACTIVO="usuarioactivo";

    public static final String DB_NAME="db_test";
    public static final int DB_VERSION=1;

    public DataManager(Context context){

        helper = new mySQLiteOpenHelper(context);
        db = helper.getWritableDatabase();
        Log.d("myLog:", "Constructor DataManager");

    }

    public void InsertUsuario(Usuario usuario){

        int usuarioId;
        usuarioId = helper.InsertUsuario(db,usuario);
        usuario.setId(usuarioId);
    }

    public void UpdateUsuario(Usuario usuario){

        helper.UpdateUsuario(db,usuario);

    }

    public Cursor QueryUsuarioById(int usuarioId){
        return helper.QueryUsuarioById(db,usuarioId);

    }

    public Cursor QueryUsuarioByCorreo(String correo){
        return helper.QueryUsuarioByCorreo(db,correo);

    }

}

class mySQLiteOpenHelper extends SQLiteOpenHelper {



    public mySQLiteOpenHelper(Context context){

        super(context,DB_NAME,null,DB_VERSION);
        //context.deleteDatabase(DB_NAME);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        CreateTables(db);
    }



    protected int InsertUsuario(SQLiteDatabase db, Usuario usuario){

        String queryId;

        String insertQuery = "insert into " + TABLA_USUARIO + " (" +
                USUARIO_NOMBRE + ","+
                USUARIO_APELLIDO      + ","+
                USUARIO_CORREO        + ","+
                USUARIO_USER          + ","+
                USUARIO_PASS          + ","+
                USUARIO_USUARIOACTIVO +
                ") VALUES ('" +
                usuario.getNombre()       + "','" +
                usuario.getApellido()     + "','" +
                usuario.getCorreo()       + "','" +
                usuario.getUser()         + "','" +
                usuario.getPass()         + "','" +
                usuario.isUsuarioactivo() + "');";

        db.execSQL(insertQuery);

        queryId = "select " + USUARIO_ID    +
                " from "  + TABLA_USUARIO +
                " where " +
                USUARIO_CORREO      +"='" +usuario.getCorreo()+"';";


        Cursor c = db.rawQuery(queryId, null);
        c.moveToFirst();

        return c.getInt(0);

    }


    //querry usuario by id


    protected Cursor QueryUsuarioById(SQLiteDatabase db, int idUsuario){
        String query = "select "         +
                USUARIO_ID               + "," +
                USUARIO_NOMBRE + "," +
                USUARIO_APELLIDO         + "," +
                USUARIO_CORREO           + "," +
                USUARIO_USER             + "," +
                USUARIO_PASS             + "," +
                USUARIO_USUARIOACTIVO    +
                " FROM "                 +
                TABLA_USUARIO            + " WHERE " +
                USUARIO_ID               + "='" +

                String.valueOf(idUsuario)  + "';";

        return db.rawQuery(query, null);
    }

    //querry usuario by id


    protected Cursor QueryUsuarioByCorreo(SQLiteDatabase db, String correo){
        String query = "select "         +
                USUARIO_ID               + "," +
                USUARIO_NOMBRE + "," +
                USUARIO_APELLIDO         + "," +
                USUARIO_CORREO           + "," +
                USUARIO_USER             + "," +
                USUARIO_PASS             + "," +
                USUARIO_USUARIOACTIVO    +
                " FROM "                 +
                TABLA_USUARIO            + " WHERE " +
                USUARIO_CORREO           + "='" +

                String.valueOf(correo)  + "';";

        return db.rawQuery(query, null);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);

    }

    /***
     * Crea las tabla si no existen en el móvil.
     * @param db
     */
    protected void CreateTables(SQLiteDatabase db){

        String createTableQuery = "create table if not exists " + TABLA_USUARIO + " (" +
                USUARIO_ID              + " integer primary key autoincrement," +                  //1
                USUARIO_NOMBRE + " text," +                                      //2
                USUARIO_APELLIDO        + " text,"+                                                //3
                USUARIO_CORREO          + " text not null,"+                                                //4
                USUARIO_USER            + " text,"+                                                //5
                USUARIO_PASS            + " text not null,"+                                                //6
                USUARIO_USUARIOACTIVO   + " text);";                                                   //15

        db.execSQL(createTableQuery);

    }


    //Update Usuario

    protected void UpdateUsuario(SQLiteDatabase db, Usuario usuario){

        String queryId;


        String updateQuery = "UPDATE " + TABLA_USUARIO + " SET " +
                USUARIO_NOMBRE         + "= '"+ usuario.getNombre() +"' ," +
                USUARIO_APELLIDO       + "= '"+ usuario.getApellido() +"' ," +
                USUARIO_CORREO         + "= '"+ usuario.getCorreo() +"' ," +
                USUARIO_USER           + "= '"+ usuario.getPass() +"' ," +
                USUARIO_PASS           + "= '"+ usuario.isUsuarioactivo() +"'"
                + " WHERE " +
                USUARIO_ID              + " = '"+ usuario.getId() + "'";


        Log.d("mySQLlog:", updateQuery);
        db.execSQL(updateQuery);

    }

    @Override
    protected void finalize() throws Throwable {
        this.close();
        super.finalize();
    }




}
