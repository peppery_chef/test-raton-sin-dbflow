package cl.palacios.www.ratontest2;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText usuario;
    private EditText contrasena;
    private Button btnGuardar;
    private Button btnBuscar;

    private String tmpUser = "";
    private DataManager dm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dm = new DataManager(this);

        usuario = findViewById(R.id.editTextUsuario);
        contrasena = findViewById(R.id.editTextPassword);
        btnBuscar = findViewById(R.id.buttonBuscar);
        btnGuardar = findViewById(R.id.buttonGuardar);

        btnGuardar.setOnClickListener(accionGuardar);
        btnBuscar.setOnClickListener(accionBuscar);
    }


    View.OnClickListener accionGuardar = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Guardar();

        }
    };

    View.OnClickListener accionBuscar = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Cargar();

        }
    };

    private void limpiar(){

        usuario.setText("");
        contrasena.setText("");

    }

    private void Guardar(){

        Cursor c = dm.QueryUsuarioByCorreo(usuario.getText().toString());

        if (c.getCount()>0){

            Toast.makeText(MainActivity.this,
                    "El usuario " + usuario.getText().toString() + " ya existe",
                    Toast.LENGTH_LONG).show();

        }else {

            Usuario usr = new Usuario();

            usr.setNombre("test nombre");
            usr.setApellido("test apellido");
            usr.setCorreo(usuario.getText().toString());
            usr.setUser(usuario.getText().toString());
            usr.setPass(contrasena.getText().toString());
            usr.setUsuarioactivo(true);

            dm.InsertUsuario(usr);

            Toast.makeText(MainActivity.this,
                    "Usuario guardado",
                    Toast.LENGTH_LONG).show();

            limpiar();

        }




    }

    private void Cargar(){

        Cursor c = dm.QueryUsuarioByCorreo(usuario.getText().toString());

        if (c.getCount() > 0 ){

            c.moveToFirst();

            tmpUser = (c.getString(3));

            Toast.makeText(MainActivity.this,
                    "Usuario " + tmpUser + "existe",
                    Toast.LENGTH_LONG).show();

        }else {

            Toast.makeText(MainActivity.this,
                    "Usuario no existe",
                    Toast.LENGTH_LONG).show();

        }

    }


}
